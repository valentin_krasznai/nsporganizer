# NSPOrganizer

This is a .NET Core program for organizing .nsp files which were downloaded with the CDNSP tool.

## Build

1. Download the source files
2. Open a command promt and navigate to the downloaded files
3. Run `dotnet publish -c Release`
4. Locate the build files in .NSPOrganizer/bin/Release/netcoreapp2.0/publish

## Usage
1. Copy the build to the folder which contains the .nsp files, open a promt in the folder, then run `dotnet NSPOrganizer.dll`
2. (Optional) Run `dotnet NSPOrganizer.dll --source c:/path/to/the/files`