namespace NSPOrganizer.Models
{
    public enum NspFileType
    {
        Base = 1,
        Update,
        Dlc,
        Demo
    }
}