using System.Linq;

namespace NSPOrganizer.Models
{
    public class NspFile
    {
        public string Name { get; set; }
        public string OriginalPath { get; set; }
        private string TitleId { get; set; }
        private string Version { get; set; }
        public NspFileType Type { get; set; }

        /// <summary>
        /// Creates an instance by parsing an NSP file's path.
        ///
        /// Example filenames:
        ///     Mario + Rabbids Kingdom Battle [010067300059a000][v0].nsp                 
        ///     Mario + Rabbids Kingdom Battle Demo [010067300059a800][v524288].nsp       
        ///     Mario + Rabbids Kingdom Battle [UPD][010067300059a800][v524288].nsp       
        ///     [DLC] Mario + Rabbids Kingdom Battle Pixel Pack [010067300059b001][v0].nsp
        /// 
        /// </summary>
        /// <param name="path">Original path to the NSP file</param>
        /// <returns>A new instance</returns>
        public static NspFile FromFileName(string path)
        {
            var filename = System.IO.Path.GetFileNameWithoutExtension(path);
            var type = NspFileType.Base;

            if (filename.Contains("[DLC]"))
            {
                type = NspFileType.Dlc;
                filename = filename.Replace("[DLC]", "");
            }
            else if (filename.Contains("[UPD]"))
            {
                type = NspFileType.Update;
                filename = filename.Replace("[UPD]", "");
            }
            else if (filename.Contains(" Demo"))
            {
                type = NspFileType.Demo;
                filename = filename.Replace(" Demo", "");
            }

            const char splitChar = '|';
            var args = filename
                .Replace("][", splitChar.ToString())
                .Replace('[', splitChar)
                .Replace(']', splitChar)
                .Split(splitChar)
                .Select(x => x.Trim())
                .Where(x => !string.IsNullOrEmpty(x))
                .ToList();

            if (args.Count != 3 || !args[2].StartsWith("v")) return null;
            
            return new NspFile
            {
                Name = args[0],
                OriginalPath = path,
                TitleId = args[1],
                Type = type,
                Version = args[2]
            };
        }

        public string ToFileName()
        {
            return $"{Name} [{(int)Type}_{Type.ToString().ToUpper()}][{TitleId}][{Version}].nsp";
        }
    }
}