using CommandLine;

namespace NSPOrganizer.Models
{
    public class Options
    {
        [Option('s', "source", Required = false, HelpText = "Path to the directory containing the NSP files. Defaults to current directory.")]
        public string SourcePath { get; set; }
    }
}