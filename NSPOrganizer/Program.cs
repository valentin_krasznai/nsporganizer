﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CommandLine;
using NSPOrganizer.Models;

namespace NSPOrganizer
{
    internal static class Program
    {
        private static Options _options;

        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(ProcessOptions)
                .WithNotParsed(errs => Environment.Exit(1));

            Console.WriteLine($"Searching for NSP files in \"{_options.SourcePath}\".");
            var nspFiles = GetNspFiles();

            Console.WriteLine($"Found {nspFiles.Count} NSP files.");
            MoveNspFiles(nspFiles);

            if (nspFiles.Any(x => x.Type == NspFileType.Dlc))
                Console.WriteLine("Please move the DLCs into the correct folders manually.");
            
            Console.WriteLine("Done!");
        }

        private static List<NspFile> GetNspFiles()
        {
            return Directory.GetFiles(_options.SourcePath)
                .Where(IsNspFile)
                .Where(IsNotAlreadyProcessed)
                .Select(NspFile.FromFileName)
                .Where(x => x != null)
                .ToList();
        }

        private static void MoveNspFiles(List<NspFile> nspFiles)
        {
            if (!nspFiles.Any())
            {
                Console.WriteLine("There's nothing to organize.");
                return;
            }
            
            foreach (var file in nspFiles)
            {
                var newFileName = file.ToFileName();
                var directoryPath = _options.SourcePath + (file.Type == NspFileType.Dlc ? "" : "\\" + file.Name);
                var filePath = directoryPath + "\\" + newFileName;
                Console.WriteLine($"Moving {file.Name} to {filePath}");

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                File.Move(file.OriginalPath, filePath);
            }
        }

        private static void ProcessOptions(Options opts)
        {
            _options = opts;

            if (string.IsNullOrEmpty(_options.SourcePath))
            {
                Console.WriteLine($"No directory is given, falling back to current directory.");
                _options.SourcePath = Directory.GetCurrentDirectory();
            }
            else if (!Directory.Exists(_options.SourcePath))
            {
                Console.WriteLine($"Given directory  \"{_options.SourcePath}\" is invalid or doesn't exist.");
                Environment.Exit(1);
            }

            _options.SourcePath.TrimEnd('\\');
        }

        private static bool IsNspFile(string file)
        {
            return Path.GetExtension(file) == ".nsp";
        }

        private static bool IsNotAlreadyProcessed(string file)
        {
            return !Regex.IsMatch(file, @"\[\d_");
        }
    }
}